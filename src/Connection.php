<?php
namespace App;
use PDO;
use PDOException;
class Connection
{
    private $user = 'root';
    private $pass = '';
    protected $con;

    public function __construct()
    {
        try {
            $this->con = new PDO('mysql:host=localhost;dbname=php_oop_crud', $this->user, $this->pass);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
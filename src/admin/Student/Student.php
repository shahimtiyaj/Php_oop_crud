<?php
namespace App\admin\Student;
session_start();
use App\Connection;
use PDOException;
use PDO;
class Student extends Connection
{
    private $user_name;
    private $user_email;
    private $user_pass;
    private $id;
    public function set($data = array()){
       if (array_key_exists('user_name', $data)) {
           $this->user_name = $data['user_name'];
       }
        if (array_key_exists('user_email', $data)) {
            $this->user_email = $data['user_email'];
        }
        if (array_key_exists('user_pass', $data)) {
            $this->user_pass = $data['user_pass'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
       return $this;
   }



   public function store(){

     try{
         $stmt = $this->con->prepare("INSERT INTO `user` (`user_name`, `user_email`,`user_pass`) VALUES(:user_name,:user_email,:user_pass)");
         $result =  $stmt->execute(array(
             ':user_name' => $this->user_name,
             ':user_email' => $this->user_email,
             ':user_pass' => $this->user_pass
         ));
         if($result){
             $_SESSION['msg'] = 'Data successfully Inserted !!';
             header('location:index.php');
         }
     }catch (PDOException $e) {
         echo "There is some problem in connection: " . $e->getMessage();
     }

   }


    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `user`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function view($id){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `user` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function delete($id){
        try{
            $stmt = $this->con->prepare("DELETE FROM `user` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['delete'] = 'Data successfully Deleted !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `php_oop_crud`.`user` SET `user_name` = :user_name, `user_email` = :user_email, `user_pass` = :user_pass WHERE `user`.`id` = :id;");
            $stmt->bindValue(':user_name', $this->user_name, PDO::PARAM_INT);
            $stmt->bindValue(':user_email', $this->user_email, PDO::PARAM_INT);
            $stmt->bindValue(':user_pass', $this->user_pass, PDO::PARAM_INT);
            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }



}
<?php
//session_start();
include_once '../../../vendor/autoload.php';

?>

<div id="page-wrapper" style="min-height: 349px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Forms</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <div style="position: fixed; z-index: 111; right: 30px">
        <?php
        if(isset($_SESSION['msg'])) {
            echo "<div class='alert alert-success'>".$_SESSION['msg']."</div>";
            session_unset();
        }
        if(isset($_SESSION['delete'])) {
            echo "<div class='alert alert-danger'>".$_SESSION['delete']."</div>";
            session_unset();
        }
        if(isset($_SESSION['update'])) {
            echo "<div class='alert alert-info'>".$_SESSION['update']."</div>";
            session_unset();
        }
        ?>
    </div>

    <!DOCTYPE html>
    <html>
    <head>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #b3c9dd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #ffffff;
            }
        </style>
    </head>
    <body>

    <table border="3">

        <table>
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Password</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $all_student = new \App\admin\Student\Student();
                        $students = $all_student->index();
                        $sl = 1;
                        foreach ($students as $student){
                            ?>
                            <tr class="gradeU">
                                <td><?php echo $sl++?></td>
                                <td><?php echo $student['user_name']?></td>
                                <td><?php echo $student['user_email']?></td>
                                <td><?php echo $student['user_pass']?></td>
                                <td class="center">
                                    <a class="text-info" href="view.php?id=<?php echo $student['id'] ?>">View</a> |
                                    <a class="text-info" href="edit.php?id=<?php echo $student['id'] ?>">Edit</a> |
                                    <a class="text-danger" href="delete.php?id=<?php echo $student['id'] ?>">Delete</a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>

    </table>

    </body>
    </html>


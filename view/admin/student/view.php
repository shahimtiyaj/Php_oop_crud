<?php

include_once '../../../vendor/autoload.php';

$student = new \App\admin\Student\Student();
$student_view = $student->view($_GET['id']);

?>


<!DOCTYPE html>
<html>
<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #b3c9dd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #ffffff;
        }
    </style>
</head>
<body>

<table border="3">

    <table >
        <tr >
            <th style="background-color: #b3c9dd" >Use Name</th>
            <th style="background-color: #0cbdbe">User Email</th>
            <th style="background-color: #b3c9dd">User Password</th>
        </tr>
        <tr>
            <td> <?php echo $student_view['user_name']?> </td>
            <td><?php echo $student_view['user_email']?> </td>
            <td> <?php echo $student_view['user_pass']?> </td>
        </tr>

    </table>

</body>
</html>


